import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'routes-denrees',
    pathMatch: 'full'
  },
  {
    path: 'liste-denrees',
    redirectTo: 'routes-denrees',
  },


  //routes de gestion des denrées

  {
    path: 'routes-denrees',
    loadChildren: () => import('./pages/denrées/routes-denrees/routes-denrees.module').then( m => m.RoutesDenreesPageModule)
  },

  //creation des denrées
  {
    path: 'create-denree-pays',
    loadChildren: () => import('./pages/denrées/denrees-pays/create-denree-pays/create-denree-pays.module').then( m => m.CreateDenreePaysPageModule)
  },
  //update des denrées
  {
    path: 'update-denree-pays/:denree',
    loadChildren: () => import('./pages/denrées/denrees-pays/update-denree-pays/update-denree-pays.module').then( m => m.UpdateDenreePaysPageModule)
  },

  //listing des denrées
  {
    path: 'liste-denrees-pays',
    loadChildren: () => import('./pages/denrées/denrees-pays/liste-denrees-pays/liste-denrees-pays.module').then( m => m.ListeDenreesPaysPageModule)
  },
  {
    path: 'listing-denree-region',
    loadChildren: () => import('./pages/denrées/denrees-region/listing-denree-region/listing-denree-region.module').then( m => m.ListingDenreeRegionPageModule)
  },
  {
    path: 'listing-denree-dept',
    loadChildren: () => import('./pages/denrées/denrees-departement/listing-denree-dept/listing-denree-dept.module').then( m => m.ListingDenreeDeptPageModule)
  },
  {
    path: "listing-denrees-secteur",
    loadChildren: () => import('./pages/denrées/denrees-secteur/listing-secteur/listing-secteur.module').then( m => m.ListingSecteurPageModule)
  },

  //tranferts des denrées

   {
    path: 'transfert-region/:denree',
    loadChildren: () => import('./pages/denrées/denrees-pays/transfert-region/transfert-region.module').then( m => m.TransfertRegionPageModule)
  },
  {
    path: 'transfert-dept/:denree',
    loadChildren: () => import('./pages/denrées/denrees-region/transfert-dept/transfert-dept.module').then( m => m.TransfertDeptPageModule)
  },
  {
    path: 'transfert-secteur/:denree',
    loadChildren: () => import('./pages/denrées/denrees-departement/transfert-secteur/transfert-secteur.module').then( m => m.TransfertSecteurPageModule)
  },
  {
    path: 'transfert-menage/:denree',
    loadChildren: () => import('./pages/denrées/denrees-secteur/transfert-menage/transfert-menage.module').then( m => m.TransfertMenagePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  // {path: "/login", component : LoginPage},
  // {path: "/logout", component : LogoutPage},

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'citoyen',
    loadChildren: () => import('./pages/citoyen/citoyen.module').then( m => m.CitoyenPageModule)
  },
  {
    path: 'travel',
    loadChildren: () => import('./pages/travel/travel.module').then( m => m.TravelPageModule)
  },
  {
    path: 'denree',
    loadChildren: () => import('./pages/denree/denree.module').then( m => m.DenreePageModule)
  },
  {
    path: 'secteur',
    loadChildren: () => import('./pages/secteur/secteur.module').then( m => m.SecteurPageModule)
  },
  {
    path: 'region',
    loadChildren: () => import('./pages/region/region.module').then( m => m.RegionPageModule)
  },
  {
    path: 'departement',
    loadChildren: () => import('./pages/departement/departement.module').then( m => m.DepartementPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./components/logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'passwd-forgot',
    loadChildren: () => import('./components/passwd-forgot/passwd-forgot.module').then( m => m.PasswdForgotPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
