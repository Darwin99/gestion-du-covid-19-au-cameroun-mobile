import { Component } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  active = '';

  NAV = [
    {name: 'Home', link: '/home', icon: 'home'},
    {name: 'Citoyen',link: '/citoyen',icon: 'person'},
    {name: 'Travel',link: '/travel',icon: 'car'},
    {name: 'Denree',link: '/routes-denrees',icon: 'fast-food'},
    {name: 'Region',link: '/region',icon: 'map'},
    {name: 'Departement',link: '/departement',icon: 'map'},
    {name: 'Secteur',link: '/secteur',icon: 'business'},
    {name: 'Menages',link: '/menages',icon: 'storefront'},
    {name: 'Statistiques',link: '/statistiques',icon: 'stats-chart'},
    {name: 'Parametres',link: '/parmetres',icon: 'settings'},
    {name: 'Deconnexion',link: '/logout',icon: 'log-out'},


  ]
  constructor(private router: Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.active = event.url
    })
  }
}


