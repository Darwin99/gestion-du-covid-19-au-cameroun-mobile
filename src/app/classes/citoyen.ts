export class CitoyenBean{
  id: string;
  login : string;
  password: string;
  nom : string;
  prenom : string;
  date_naissance: string;
  sexe: string;
  image: string;
  profession: string;
  est_contamine: boolean;
  id_agent: string;
  id_menage: string;
}