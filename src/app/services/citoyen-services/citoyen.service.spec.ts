import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CitoyenBean } from './../../classes/citoyen';

@Injectable({
  providedIn: 'root'
})
export class MessageApiService {
// -- les urls
private apiURI       = 'http://10.42.0.1:8091/CovidProject/rest/';


private  action: CitoyenBean;

  constructor(public  http: HttpClient) { }

 


    // methode de du service citoyen qui feetch toutes les donnees de la base de donnes
    //citizens/
  getAllMessage(table_name){
    return this.http.get<[CitoyenBean]>(this.apiURI+table_name);
  }

}
