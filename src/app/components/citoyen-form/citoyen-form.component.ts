import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-citoyen-form',
  templateUrl: './citoyen-form.component.html',
  styleUrls: ['./citoyen-form.component.scss'],
})
export class CitoyenFormComponent implements OnInit {


  dataForm!: FormGroup;

  constructor(private citizenServices: CitizenService, private formBuilder: FormBuilder) { }

  ngOnInit() { }




  get form() {
    return this.dataForm.controls;
  }
  onSubmit() {
    console.log('on submit');

    if (this.dataForm.invalid) {
      alert('tous les champs en rouge sont obligatoire')
      return;
    }

    const formData = new FormData();
    formData.append('nom', '' + this.form.nom.value);
    formData.append('login', '' + this.form.login.value);
    formData.append('password', '' + this.form.password.value);
    formData.append('prenom', '' + this.form.prenom.value);
    formData.append('profession', '' + this.form.profession.value);
    formData.append('dateDeNaissance', '' + this.form.dateDeNaissance.value);

    formData.append('estcontamine', '' + this.form.estContamine.value);
    formData.append('id_agent', '' + this.form.id_agent.value);
    formData.append('id_menage', '' + this.form.id_menage.value);


    console.log(formData);
    this.citizenServices.add(formData)
      .then(resp => {
        alert('enregistrer avec success')
        this.dataForm.reset();
      })
      .catch(err => {
        console.log(err)
        alert('enregistrement a echoue')
      });
  }
}
