import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswdForgotPage } from './passwd-forgot.page';

const routes: Routes = [
  {
    path: '',
    component: PasswdForgotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswdForgotPageRoutingModule {}
