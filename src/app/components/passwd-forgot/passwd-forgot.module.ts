import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswdForgotPageRoutingModule } from './passwd-forgot-routing.module';

import { PasswdForgotPage } from './passwd-forgot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswdForgotPageRoutingModule
  ],
  declarations: [PasswdForgotPage]
})
export class PasswdForgotPageModule {}
