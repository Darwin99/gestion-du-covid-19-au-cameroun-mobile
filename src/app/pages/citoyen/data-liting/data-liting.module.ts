import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataLitingPageRoutingModule } from './data-liting-routing.module';

import { DataLitingPage } from './data-liting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataLitingPageRoutingModule
  ],
  declarations: [DataLitingPage]
})
export class DataLitingPageModule {}
