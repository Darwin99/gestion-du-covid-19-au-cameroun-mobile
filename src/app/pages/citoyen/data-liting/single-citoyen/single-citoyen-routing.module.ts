import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleCitoyenPage } from './single-citoyen.page';

const routes: Routes = [
  {
    path: '',
    component: SingleCitoyenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleCitoyenPageRoutingModule {}
