import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingleCitoyenPageRoutingModule } from './single-citoyen-routing.module';

import { SingleCitoyenPage } from './single-citoyen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleCitoyenPageRoutingModule
  ],
  declarations: [SingleCitoyenPage]
})
export class SingleCitoyenPageModule {}
