import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataLitingPage } from './data-liting.page';

const routes: Routes = [
  {
    path: '',
    component: DataLitingPage
  },
  {
    path: 'single',
    loadChildren: () => import('./single-citoyen/single-citoyen.module').then( m => m.SingleCitoyenPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataLitingPageRoutingModule {}
