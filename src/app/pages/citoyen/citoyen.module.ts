import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CitoyenPageRoutingModule } from './citoyen-routing.module';

import { CitoyenPage } from './citoyen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CitoyenPageRoutingModule
  ],
  declarations: [CitoyenPage]
})
export class CitoyenPageModule {}
