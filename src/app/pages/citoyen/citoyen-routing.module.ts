import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CitoyenPage } from './citoyen.page';

const routes: Routes = [
  {
    path: '',
    component: CitoyenPage
  },
  {
    path: 'form-add',
    loadChildren: () => import('./form-add/form-add.module').then( m => m.FormAddPageModule)
  },
  {
    path: 'data-liting',
    loadChildren: () => import('./data-liting/data-liting.module').then( m => m.DataLitingPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CitoyenPageRoutingModule {}
