import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DenreePage } from './denree.page';

const routes: Routes = [
  {
    path: '',
    component: DenreePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DenreePageRoutingModule {}
