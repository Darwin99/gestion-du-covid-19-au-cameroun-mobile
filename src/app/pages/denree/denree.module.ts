import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DenreePageRoutingModule } from './denree-routing.module';

import { DenreePage } from './denree.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DenreePageRoutingModule
  ],
  declarations: [DenreePage]
})
export class DenreePageModule {}
