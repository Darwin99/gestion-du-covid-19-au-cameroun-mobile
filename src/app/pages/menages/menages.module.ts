import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenagesPageRoutingModule } from './menages-routing.module';

import { MenagesPage } from './menages.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenagesPageRoutingModule
  ],
  declarations: [MenagesPage]
})
export class MenagesPageModule {}
