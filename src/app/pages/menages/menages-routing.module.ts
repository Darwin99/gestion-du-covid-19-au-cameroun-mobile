import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenagesPage } from './menages.page';

const routes: Routes = [
  {
    path: '',
    component: MenagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenagesPageRoutingModule {}
