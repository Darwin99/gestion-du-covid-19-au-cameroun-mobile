import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransfertSecteurPageRoutingModule } from './transfert-secteur-routing.module';

import { TransfertSecteurPage } from './transfert-secteur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransfertSecteurPageRoutingModule
  ],
  declarations: [TransfertSecteurPage]
})
export class TransfertSecteurPageModule {}
