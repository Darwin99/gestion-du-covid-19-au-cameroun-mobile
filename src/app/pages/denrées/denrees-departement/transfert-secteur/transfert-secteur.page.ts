import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-transfert-secteur',
  templateUrl: './transfert-secteur.page.html',
  styleUrls: ['./transfert-secteur.page.scss'],
})
export class TransfertSecteurPage implements OnInit {

  idDenree : string;
  idSecteur: string;
  quantite: any;

  denrees = [
    {
      id : 17634,
      nom : "Riz",
    },
    {
      id : 87252,
      nom : "Speghetti",
    }
  ]
  secteurs = [
    {
      id: 93616,
      nom: "penka-michel",
    },
    {
      id: 82524,
      nom: "DoualaVe",
    },
    {
      id: 92624,
      nom: "bafoussamIer",
    },
    {
      id: 57826,
      nom: "yaounde1",
    }
  ]
  constructor(
      private route: ActivatedRoute,
      private router: Router,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {this.idDenree = params['denree']})
  }
  submit(){
    console.log(this.idDenree +" "+ this.idSecteur+" "+ this.quantite)
  }


}
