import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfertSecteurPage } from './transfert-secteur.page';

const routes: Routes = [
  {
    path: '',
    component: TransfertSecteurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfertSecteurPageRoutingModule {}
