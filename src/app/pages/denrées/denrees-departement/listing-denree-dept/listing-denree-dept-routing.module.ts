import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListingDenreeRegionPage } from './listing-denree-dept.page';

const routes: Routes = [
  {
    path: '',
    component: ListingDenreeRegionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListingDenreeDeptPageRoutingModule {}

