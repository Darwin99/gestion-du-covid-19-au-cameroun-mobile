import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListingDenreeDeptPageRoutingModule } from './listing-denree-dept-routing.module';

import { ListingDenreeRegionPage } from './listing-denree-dept.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListingDenreeDeptPageRoutingModule
  ],
  declarations: [ListingDenreeRegionPage]
})
export class ListingDenreeDeptPageModule {}
