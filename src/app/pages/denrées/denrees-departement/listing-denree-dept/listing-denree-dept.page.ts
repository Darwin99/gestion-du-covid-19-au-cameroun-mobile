import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-listing-denree-dept',
  templateUrl: './listing-denree-dept.page.html',
  styleUrls: ['./listing-denree-dept.page.scss'],
})
export class ListingDenreeRegionPage implements OnInit {

  departement;
  datas = [
    {
      id: 87654,
      nom: "Riz",
      quantite: 2500,
      categorie: "Sac de 50Kg",
      secteur:"menoua"
    },
    {
      id: 97124,
      nom: "Spaghetti",
      quantite: 750,
      categorie: "Paquet de 500g",
      secteur:"menoua"
    }
  ]
  constructor(public alertController: AlertController) { }

  ngOnInit() {
    this.departement = "Menoua";
  }
  async presentAlert(mes) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Informations',
      message: mes,
      buttons: ['OK']
    });

    await alert.present();
  }
  details(ms: any){
      this.presentAlert(ms);
  }

}
