import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListingSecteurPageRoutingModule } from './listing-secteur-routing.module';

import { ListingSecteurPage } from './listing-secteur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListingSecteurPageRoutingModule
  ],
  declarations: [ListingSecteurPage]
})
export class ListingSecteurPageModule {}
