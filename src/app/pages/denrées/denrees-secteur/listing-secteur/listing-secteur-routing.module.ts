import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListingSecteurPage } from './listing-secteur.page';

const routes: Routes = [
  {
    path: '',
    component: ListingSecteurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListingSecteurPageRoutingModule {}
