import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-listing-secteur',
  templateUrl: './listing-secteur.page.html',
  styleUrls: ['./listing-secteur.page.scss'],
})
export class ListingSecteurPage implements OnInit {

  secteur: any;
  datas = [
    {
      id: 87654,
      nom: "Riz",
      quantite: 2500,
      categorie: "Sac de 50Kg",
    },
    {
      id: 97124,
      nom: "Spaghetti",
      quantite: 750,
      categorie: "Paquet de 500g",
    }
  ]
  constructor(public alertController: AlertController) { }

  ngOnInit() {
    this.secteur = "Dschang";
  }
  async presentAlert(mes) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Informations',
      message: mes,
      buttons: ['OK']
    });

    await alert.present();
  }
  details(ms: any){
      this.presentAlert(ms);
  }


}
