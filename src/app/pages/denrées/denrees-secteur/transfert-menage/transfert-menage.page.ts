import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-transfert-menage',
  templateUrl: './transfert-menage.page.html',
  styleUrls: ['./transfert-menage.page.scss'],
})
export class TransfertMenagePage implements OnInit {

  idDenree : string;
  idMenage: string;
  quantite: any;

  denrees = [
    {
      id : 17634,
      nom : "Riz",
    },
    {
      id : 87252,
      nom : "Speghetti",
    }
  ]
  menages = [
    {
      id: 93616,
      nom: "Famille B",
    },
    {
      id: 82524,
      nom: "Famille B",
    },
    {
      id: 92624,
      nom: "Famille B",
    },
    {
      id: 57826,
      nom: "Famille B",
    }
  ]
  constructor(
      private route: ActivatedRoute,
      private router: Router,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {this.idDenree = params['denree']})
  }
  submit(){
    console.log(this.idDenree +" "+ this.idMenage+" "+ this.quantite)
  }

}
