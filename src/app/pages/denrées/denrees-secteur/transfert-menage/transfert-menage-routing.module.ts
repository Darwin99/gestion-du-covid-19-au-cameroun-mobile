import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfertMenagePage } from './transfert-menage.page';

const routes: Routes = [
  {
    path: '',
    component: TransfertMenagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfertMenagePageRoutingModule {}
