import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransfertMenagePageRoutingModule } from './transfert-menage-routing.module';

import { TransfertMenagePage } from './transfert-menage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransfertMenagePageRoutingModule
  ],
  declarations: [TransfertMenagePage]
})
export class TransfertMenagePageModule {}
