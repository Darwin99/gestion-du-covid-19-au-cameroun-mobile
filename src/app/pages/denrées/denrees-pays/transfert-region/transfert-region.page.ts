import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transfert-region',
  templateUrl: './transfert-region.page.html',
  styleUrls: ['./transfert-region.page.scss'],
})
export class TransfertRegionPage implements OnInit {

  idDenree : string;
  idRegion: string;
  quantite: any;

  denrees = [
    {
      id : 17634,
      nom : "Riz",
    },
    {
      id : 87252,
      nom : "Speghetti",
    }
  ]
  regions = [
    {
      id: 93616,
      nom: "Ouest",
    },
    {
      id: 82524,
      nom: "Centre",
    },
    {
      id: 92624,
      nom: "Adamaoua",
    },
    {
      id: 57826,
      nom: "Sud",
    }
  ]
  constructor(
      private route: ActivatedRoute,
      private router: Router,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {this.idDenree = params['denree']})
  }
  submit(){
    console.log(this.idDenree +" "+ this.idRegion+" "+ this.quantite)
  }

}
