import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfertRegionPage } from './transfert-region.page';

const routes: Routes = [
  {
    path: '',
    component: TransfertRegionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfertRegionPageRoutingModule {}
