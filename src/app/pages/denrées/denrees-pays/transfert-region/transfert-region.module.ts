import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransfertRegionPageRoutingModule } from './transfert-region-routing.module';

import { TransfertRegionPage } from './transfert-region.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransfertRegionPageRoutingModule
  ],
  declarations: [TransfertRegionPage]
})
export class TransfertRegionPageModule {}
