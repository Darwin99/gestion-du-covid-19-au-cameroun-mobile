import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateDenreePaysPageRoutingModule } from './create-denree-pays-routing.module';

import { CreateDenreePaysPage } from './create-denree-pays.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateDenreePaysPageRoutingModule
  ],
  declarations: [CreateDenreePaysPage]
})
export class CreateDenreePaysPageModule {}
