import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateDenreePaysPage } from './create-denree-pays.page';

const routes: Routes = [
  {
    path: '',
    component: CreateDenreePaysPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateDenreePaysPageRoutingModule {}
