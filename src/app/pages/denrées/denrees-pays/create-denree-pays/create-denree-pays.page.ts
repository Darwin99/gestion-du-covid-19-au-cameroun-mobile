import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-denree-pays',
  templateUrl: './create-denree-pays.page.html',
  styleUrls: ['./create-denree-pays.page.scss'],
})
export class CreateDenreePaysPage implements OnInit {

  nomDenree: string;
  quantite: number;
  categorie: string;
  categorie1: string;
  constructor() { }

  ngOnInit() {
  }
  submit(){
    if(this.categorie == 'autre'){
      this.categorie = this.categorie1;
    }
    console.log(this.nomDenree +" "+ this.quantite +" "+ this.categorie)
  }

}
