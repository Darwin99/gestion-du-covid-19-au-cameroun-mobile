import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeDenreesPaysPage } from './liste-denrees-pays.page';

const routes: Routes = [
  {
    path: '',
    component: ListeDenreesPaysPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeDenreesPaysPageRoutingModule {}
