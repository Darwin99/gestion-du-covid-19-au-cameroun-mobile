import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-liste-denrees-pays',
  templateUrl: './liste-denrees-pays.page.html',
  styleUrls: ['./liste-denrees-pays.page.scss'],
})

export class ListeDenreesPaysPage implements OnInit {

  pays;

  datas = [
    {
      id: 87654,
      nom: "Riz",
      quantite: 2500,
      categorie: "Sac de 50Kg"
    },
    {
      id: 97124,
      nom: "Spaghetti",
      quantite: 750,
      categorie: "Paquet de 500g"
    }
  ]

  constructor(
    public alertController: AlertController
  ) { }

  ngOnInit() {
    this.pays = "Cameroun";
  }
  async presentAlert(mes) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Informations',
      message: mes,
      buttons: ['OK']
    });

    await alert.present();
  }
  details(ms: any){
      this.presentAlert(ms);
  }
}
