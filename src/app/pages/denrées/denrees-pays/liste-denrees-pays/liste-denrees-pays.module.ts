import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeDenreesPaysPageRoutingModule } from './liste-denrees-pays-routing.module';

import { ListeDenreesPaysPage } from './liste-denrees-pays.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeDenreesPaysPageRoutingModule
  ],
  declarations: [ListeDenreesPaysPage]
})
export class ListeDenreesPaysPageModule {}
