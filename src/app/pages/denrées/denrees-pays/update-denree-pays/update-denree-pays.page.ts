import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-denree-pays',
  templateUrl: './update-denree-pays.page.html',
  styleUrls: ['./update-denree-pays.page.scss'],
})
export class UpdateDenreePaysPage implements OnInit {

  idDenree : string;
  nomDenree: string;
  quantite: number;
  categorie: string;
  categorie1: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {this.idDenree = params['denree']})
    this.categorie="kg",
    this.nomDenree="Riz",
    this.quantite= 25;
  }
  submit(){
    console.log(this.nomDenree +" "+ this.quantite+" "+ this.categorie)
}
}
