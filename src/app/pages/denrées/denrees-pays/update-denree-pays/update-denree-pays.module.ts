import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateDenreePaysPageRoutingModule } from './update-denree-pays-routing.module';

import { UpdateDenreePaysPage } from './update-denree-pays.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateDenreePaysPageRoutingModule
  ],
  declarations: [UpdateDenreePaysPage]
})
export class UpdateDenreePaysPageModule {}
