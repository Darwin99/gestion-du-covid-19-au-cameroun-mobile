import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateDenreePaysPage } from './update-denree-pays.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateDenreePaysPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateDenreePaysPageRoutingModule {}
