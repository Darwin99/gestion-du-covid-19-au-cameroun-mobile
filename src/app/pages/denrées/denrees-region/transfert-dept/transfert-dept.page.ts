import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-transfert-dept',
  templateUrl: './transfert-dept.page.html',
  styleUrls: ['./transfert-dept.page.scss'],
})
export class TransfertDeptPage implements OnInit {

  idDenree : string;
  idDepartement: string;
  quantite: any;

  denrees = [
    {
      id : 17634,
      nom : "Riz",
    },
    {
      id : 87252,
      nom : "Speghetti",
    }
  ]
  departements = [
    {
      id: 93616,
      nom: "wouri",
    },
    {
      id: 82524,
      nom: "Menoua",
    },
    {
      id: 92624,
      nom: "Mifi",
    },
    {
      id: 57826,
      nom: "Moungo",
    }
  ]
  constructor(
      private route: ActivatedRoute,
      private router: Router,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {this.idDenree = params['denree']})
  }
  submit(){
    console.log(this.idDenree +" "+ this.idDepartement+" "+ this.quantite)
  }

}
