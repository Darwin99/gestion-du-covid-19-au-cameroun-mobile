import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfertDeptPage } from './transfert-dept.page';

const routes: Routes = [
  {
    path: '',
    component: TransfertDeptPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfertDeptPageRoutingModule {}
