import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransfertDeptPageRoutingModule } from './transfert-dept-routing.module';

import { TransfertDeptPage } from './transfert-dept.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransfertDeptPageRoutingModule
  ],
  declarations: [TransfertDeptPage]
})
export class TransfertDeptPageModule {}
