import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-listing-denree-region',
  templateUrl: './listing-denree-region.page.html',
  styleUrls: ['./listing-denree-region.page.scss'],
})
export class ListingDenreeRegionPage implements OnInit {
  region;
  datas = [
    {
      id: 87654,
      nom: "Riz",
      quantite: 2500,
      categorie: "Sac de 50Kg",
    },
    {
      id: 97124,
      nom: "Spaghetti",
      quantite: 750,
      categorie: "Paquet de 500g",
    }
  ]
  constructor(public alertController: AlertController) { }

  ngOnInit() {
    this.region = "Ouest";
  }
  async presentAlert(mes) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Informations',
      message: mes,
      buttons: ['OK']
    });

    await alert.present();
  }
  details(ms: any){
      this.presentAlert(ms);
  }

}
