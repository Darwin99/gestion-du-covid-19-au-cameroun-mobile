import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListingDenreeRegionPageRoutingModule } from './listing-denree-region-routing.module';

import { ListingDenreeRegionPage } from './listing-denree-region.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListingDenreeRegionPageRoutingModule
  ],
  declarations: [ListingDenreeRegionPage]
})
export class ListingDenreeRegionPageModule {}
