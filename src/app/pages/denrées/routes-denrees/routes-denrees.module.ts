import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoutesDenreesPageRoutingModule } from './routes-denrees-routing.module';

import { RoutesDenreesPage } from './routes-denrees.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoutesDenreesPageRoutingModule
  ],
  declarations: [RoutesDenreesPage]
})
export class RoutesDenreesPageModule {}
