import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutesDenreesPage } from './routes-denrees.page';

const routes: Routes = [
  {
    path: '',
    component: RoutesDenreesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutesDenreesPageRoutingModule {}
